## Otter stuff

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
import datetime
from twilio.rest import Client 
import time
from selenium.webdriver.firefox.options import Options
from dotenv import load_dotenv

load_dotenv()

import os 
base_path = os.path.dirname(os.path.realpath(__file__))
print("Directory path: ", base_path)

# Note: On EC2 server, time is in UTC (JHB minus two hours)
timenow = datetime.datetime.now().time()
lower = datetime.time(4,59)
upper = datetime.time(23,59)

# If the current time is before 7am or after 10pm, exit the script without executing
print("The time now is: ", timenow)
if timenow < lower or timenow > upper:
    print("Out of the right timezone, exiting the script")
    exit()

# Otherwise run the otter script
print("Starting the Otter Script ...")
options = Options()
options.headless = True

today = datetime.datetime.today().strftime('%d %B %Y')


driver = webdriver.Firefox(options=options, executable_path=r'/usr/local/bin/geckodriver')
driver.implicitly_wait(10)
driver.get("https://www.sanparks.org/parks/garden_route/camps/storms_river/tourism/availability_dates.php?id=396&resort=26&only_trails=otter&range=month")
print("Navigating to Otter availability page ...")

time.sleep(5)

print("Removing cookie pop up ...")
cookie_popup = driver.find_element(By.CLASS_NAME,"wpcc-btn")
if cookie_popup.text == "Accept":
    cookie_popup.click()

print("Selecting date range ...")


from_date = Select(driver.find_element(By.NAME, "from_date"))
from_date.select_by_visible_text(today)
print("Selected from date")

to_date = Select(driver.find_element(By.NAME, "to_date"))
to_date.select_by_visible_text("31 December 2023")
print("Selected to date")


check_availabilty = driver.find_element(By.NAME, "Submit")
check_availabilty.click()
print("Checking availability ...")



time.sleep(10)

print("Printing results ...")
results = driver.find_element(By.ID, 'results')
rows = results.find_elements(By.TAG_NAME, "tr")


# Get all dates one which Otter tickets are available
dates = [] 
for row in rows:
    cols = row.find_elements(By.TAG_NAME, "td")
    if cols:
        date = cols[0].text
        if cols[1].text == "Yes":
            dates.append(date)
        else:
            pass

# print("Dates: ", dates)

available_dates_file = os.path.join(base_path, 'available.txt')
print("Available dates file: ", available_dates_file)
# Fetch already retrieved availability for text file
with open(available_dates_file) as file:
    lines = file.readlines()
    lines = [line.rstrip() for line in lines]

# Exclude already fetched dates from the list of dates
new_dates = []
if dates:
    for date in dates:
        if date in lines:
            pass
        else:
            new_dates.append(date)

print("New available dates: ", new_dates)

# If there are new dates, send SMS with dates and add to available dates text file
if new_dates:
    # Twilio credentials
    account_sid = os.environ['TWILIO_SID']
    auth_token=os.environ['TWILIO_AUTH_TOKEN']
    client = Client(account_sid, auth_token) 
    body = 'Otter availability on {}'.format(",\n".join(new_dates))
    message = client.messages.create( 
                    from_='+13862516902',
                    # from_='whatsapp:+14155238886',
                    body=body,      
                    to='+27716416146' 
                    # to='whatsapp:+27716416146'
                ) 
    print(body)
    print(message.sid)

    # Add the new dates to the available dates file
    with open(available_dates_file, 'a') as file:
        for date in new_dates:
            file.write(f'{date}\n')

else:
    print("No new availability.")

## Send Twilio Message

driver.quit()



 
