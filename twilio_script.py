import os
from twilio.rest import Client

from dotenv import load_dotenv

load_dotenv()

account_sid = os.environ['TWILIO_SID']
auth_token=os.environ['TWILIO_AUTH_TOKEN']
client = Client(account_sid, auth_token)


message = client.messages \
                .create(
                     body="Andrew testing 123 ...",
                    #  from_='whatsapp:+14155238886',
                     from_='+13862516902',
                    #  to='whatsapp:+27716416146'
                     to='+27716416146'
                 )

print(message.sid)

