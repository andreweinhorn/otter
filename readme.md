# Getting started

virtualenv venv    
source venv/bin/activate    
pip install -r requirements.txt    
python script.py    

Note:  You will need to install selenium before this script can run.

## Installing selenium 

SELENIUM 

wget https://github.com/mozilla/geckodriver/releases/download/v0.31.0/geckodriver-v0.31.0-macos.tar.gz
tar xvfz geckodriver-v0.31.0-macos.tar.gz    
mv geckodriver /usr/local/bin


## Notes

Ensure that the "to date" in the script is set to a date in the future.    
Available otter dates will be written to the availability.txt file created by the script.  

# Using cronjob to run script

To run the script once off on my mac I can run the following from terminal:

```
<path to python interprete> <path to python script>
```
Or in my specific case:

```
/Users/Andrew/Personal/Projects/062-otter-trail-poll/venv/bin/python /Users/Andrew/Personal/Projects/062-otter-trail-poll/script.py
```

To run the script repeatedly every minute and output the logs of the cronjob to a logs folder, use the following command: 


```
*/1 * * * * /Users/Andrew/Personal/Projects/062-otter-trail-poll/venv/bin/python /Users/Andrew/Personal/Projects/062-otter-trail-poll/script.py > /Users/Andrew/Personal/Projects/062-otter-trail-poll/logs/`date +\%Y-\%m-\%d_\%H:\%M:\%S`-cron.log 2>&1
```

### Crontab commands

```
crontab -l		                	(list all cronjobs)    
crontab -r			                (remove all cronjobs)    
crontab -e 		                	(edit cronjobs)    

crontab -l > cronbackup.txt			(create back up of cronjobs)    
crontab -r    
crontab cronbackup.txt    
crontab -l    

DISPLAY=:0
```

# Setting up to run on AWS EC2

The following steps allow the script to be run repeatedly from an AWS server, and thus do no require one's laptop to be open all the time.  

1.  Spin up an AWS EC2 instance
2.  SSH into the EC2 instance 
3.  Install python3, firefox and geckodriver

```
# Show details about EC2 instance
uname -a			                      

# Install python3
yum check-update                          
yum list installed | grep -i python3    

# Install firefox
sudo amazon-linux-extras install firefox 

# Install geckodriver
wget https://github.com/mozilla/geckodriver/releases/download/v0.31.0/geckodriver-v0.31.0-linux64.tar.gz
sudo tar -xvf geckodriver-v0.31.0-linux64.tar.gz # install geckodriver
```


4.  Setup virtual environment and install requirements 

```
python3 -m venv otter/env    
cd otter     
source env/bin/activate      
pip install pip --upgrade    

# Copy requirements onto server and install

touch requirements.txt		
nano requirements.txt		   (edit requirements.txt file using nano and copy paste in)
cat requirements.txt
pip install -r requirements.txt

# Copy script onto server

touch otter.py 
nano otter.py               (edit otter.py file using nano and copy paste in otter script)
cat oter.py
```

5.  Setup the cronjob and start it running. 

Open the list of cronjobs for editing using `crontab -e` (e for edit).  
Copy paste the following cron command to run the script every 15 minutes:

```
*/15 * * * * /home/ec2-user/otter/env/bin/python3 /home/ec2-user/otter/otter.py > /home/ec2-user/otter/logs/`date +\%Y-\%m-\%d_\%H:\%M:\%S`-cron.log 2>&1
```
Set the cronjob running using `sudo service crond start`




# SSH into EC@

In order to ssh onto the EC2 running the otter trail script:

```
sudo ssh -i <pem file> <username>@<ec2 url>
```

The username on an linux instance is typically ec2-user and the url of the EC2 instance at present is ec2-52-59-231-213.eu-central-1.compute.amazonaws.com so use the following command to ssh into the ec2 terminal.  You can find the EC2 URL by logging into you AWS account and looking for the Otter EC2.  

```
sudo ssh -i /Users/Andrew/.ssh/otter-trail-server.pem ec2-user@ec2-52-59-231-213.eu-central-1.compute.amazonaws.com
```

